# Hi, mom!

I created this with Angular CLI and I just wanted something simple to play with to get the concept down before leveraging it over to a larger app.  Basically, I needed to have an array to control the columns for a grid, have the columns each be a different width (with that fr value editable within each array object), and have the grid remove columns at certain window widths.  I originally had this with a bunch of @media in my scss (in the larger project) and it just wasn't very scalable or quick to update/change/reorder/etc.

Doubtful anyone else stumbles upon this and it helps them, but if so, awesome!

Shoutout to Q.
