import { Component, OnInit } from '@angular/core'

interface Column {
    name : string
    width : string
    ///
    /// optional keys
    ///
    alwaysVisible? : boolean // optional - set to true to never remove the column
    removeOrder? : number // optional and need to be unique if any added, smallest will be removed first
    ///
    /// do not add any of the below to the objects in the columns array
    ///
    visible? : boolean // added and adjusted dynamically
    removedAt? : number // added dynamically
    originalReverseIndex?: number // added dynamically for sorting / remove order
}

interface NgStyleObject {
    'grid-template-columns'? : string
}

@Component({
    selector: 'sdgc-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

    private firstCutoff = 800
    private eachCutoff = 100

    columns : Column[] = [
        {
            name: 'Col 1',
            width: '1fr',
            alwaysVisible: true
        },{
            name: 'Col 2',
            width: '2fr',
            alwaysVisible: true
        },{
            name: 'Col 3',
            width: '1fr',
            alwaysVisible: true
        },{
            name: 'Col 4',
            width: '2fr',
            removeOrder: 1
        },{
            name: 'Col 5',
            width: '1.1fr',
            removeOrder: 2
        },{
            name: 'Col 6',
            width: '1.2fr'
        },{
            name: 'Col 7',
            width: '1.3fr',
            alwaysVisible: true
        }
    ]

    gridStyle : NgStyleObject = {}

    ngOnInit() {
        this.initCols()
        this.updateColumnsAndGridStyle(window.innerWidth)
    }

    private initCols() : void {

        let numbRemoved = 0
        const totalMinusOne = this.columns.length - 1

        this.columns.forEach((c, i) => {
            c.originalReverseIndex = totalMinusOne - i
        })

        this.columns.sort((a, b) => {

            const sortValA = a.removeOrder || a.originalReverseIndex
            const sortValB = b.removeOrder || b.originalReverseIndex

            if(a.removeOrder && !b.removeOrder) {
                return -1
            }else if(!a.removeOrder && b.removeOrder) {
                return 1
            }

            return sortValA - sortValB
        })

        this.columns.forEach(c => {

            c.visible = true

            if(!c.alwaysVisible) {
                c.removedAt = this.firstCutoff - (numbRemoved++ * this.eachCutoff)
            }
        })

        this.columns.sort((a, b) => {
            return b.originalReverseIndex - a.originalReverseIndex
        })
    }

    private updateColumnsAndGridStyle(width : number) : void {

        let allFrs = ''

        this.columns.forEach(c => {
            c.visible = c.alwaysVisible || width > c.removedAt
            if(c.visible) {
                allFrs += allFrs.length == 0 ? `${c.width}` : ` ${c.width}`
            }
        })

        this.gridStyle = {
            'grid-template-columns': allFrs
        }
    }

    onResize(evt : Event) : void {
        const w = <Window>evt.target
        this.updateColumnsAndGridStyle(w.innerWidth)
    }
}
